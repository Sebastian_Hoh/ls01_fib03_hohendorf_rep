﻿import java.util.Scanner;

class Fahrkartenautomat
{
	
	 static Scanner tastatur = new Scanner(System.in);
	
	public static double fahrkartenErstellungErfassen(double zuZahlenderBetrag) {
		
		
		System.out.println("Wie viele Tickets möchten Sie kaufen: ");
	     int ticketAnzahl = tastatur.nextInt();
	      // System.out.print("Zu zahlender Betrag (EURO): ");
	      // zuZahlenderBetrag = tastatur.nextDouble();
	       zuZahlenderBetrag = ticketAnzahl * zuZahlenderBetrag;
		
		return zuZahlenderBetrag;
	}
	
	public static double fahrkartenBezahlen(double zuZahlenderBetrag, double eingezahlterGesamtbetrag){
		//System.out.println("dddddd");
		 eingezahlterGesamtbetrag = 0.00;
		double eingeworfeneMünze;
	       
	       while(eingezahlterGesamtbetrag < zuZahlenderBetrag)
	       {
	    	   //System.out.println("Noch zu zahlen: " + (zuZahlenderBetrag - eingezahlterGesamtbetrag));
	    	   System.out.println("\n");
	    	   System.out.printf("Noch %.2f€ zu zahlen: ",  zuZahlenderBetrag - eingezahlterGesamtbetrag);
	    	   System.out.print("Eingabe (mind. 5Ct, höchstens 2 Euro): ");
	    	   eingeworfeneMünze = tastatur.nextDouble();
	           eingezahlterGesamtbetrag += eingeworfeneMünze;
	       }
		
		
		return eingezahlterGesamtbetrag;
		
	}
	
	public static double fahrkartenBestellungErfassen(double zuZahlenderBetrag) {
			zuZahlenderBetrag = 0.00;
			String[] fahrkarten = new String[]{"0. Einzelfahrschein Berlin AB","1. Einzelfahrschein Berlin BC","2. Einzelfahrschein Berlin ABC",
					"3. Kurzstrecke","4. Tageskarte Berlin AB","5. Tageskarte Berlin BC","6. Tageskarte Berlin ABC",
					"7. Kleingruppen-Tageskarte Berlin AB","8. Kleingruppen-Tageskarte Berlin BC","9. Kleingruppen-Tageskarte Berlin ABC"};
			double[] preise = new double[]{2.90 ,3.30 ,3.60 ,1.90 ,8.60 ,9.00 ,9.60 ,23.50 ,24.30 ,24.90};
			
			for(int f = 0; f < fahrkarten.length; f++) {	
					System.out.printf(" %-37s  %.2f€ \n",fahrkarten[f] , preise[f]);			
		}
		
			boolean standZurAuswahl = false;
			
			while(standZurAuswahl == false) {
				System.out.println("99. Exit");
    		int intSwitch = tastatur.nextInt();
    	
    		
    			if(intSwitch == 99) {
    				System.out.println("Good Bye");
    				System.exit(0);
    			}
    				
    			if(intSwitch != fahrkarten.length) {
    				zuZahlenderBetrag = zuZahlenderBetrag + preise[intSwitch];
    				System.out.printf("Eins macht %.2f€. ",zuZahlenderBetrag);
    				standZurAuswahl = true;
    				break;
    			}
    				
    			else{
		            System.out.println("stand nicht zur Auswahl.\nBitte geb eine neue Zahl ein:");
		            
    		}   
    		return zuZahlenderBetrag;
    		
		}
			
			
		return zuZahlenderBetrag;
		
	}
	
	public static void fahrkartenAusgeben() {
	       System.out.println("\nFahrschein wird ausgegeben");
	      /*
	       for (int i = 0; i < 8; i++)
	       {
	          System.out.print("=");
	          try {
				Thread.sleep(250);
			} catch (InterruptedException e) {
				// TODO Auto-generated catch block
				e.printStackTrace();
			}
	       }*/
	       System.out.println("\n\n");
	       
	    }
	
	public static void warten (int millisekunden) {
		
		for (int i = 0; i < 8; i++)
	       {
	          System.out.print("=");
		try {
			Thread.sleep(millisekunden);
		} catch (InterruptedException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
	       }
		System.out.println("\n\n");
	}
	
	public static double rueckgeldAusgeben(double rückgabebetrag, double eingezahlterGesamtbetrag, double zuZahlenderBetrag){
		
		 rückgabebetrag = eingezahlterGesamtbetrag - zuZahlenderBetrag;
	       if(rückgabebetrag > 0.00)
	       {
	    	   System.out.printf("Der Rückgabebetrag in Höhe von %.2f€ " , rückgabebetrag );
	    	   System.out.println("wird in folgenden Münzen ausgezahlt:");

	           while(rückgabebetrag >= 2.00) // 2 EURO-Münzen
	           {
	        	  System.out.println("2 EURO");
		          rückgabebetrag -= 2.00;
		          rückgabebetrag= Math.round(rückgabebetrag *100.0) /100.0 ;
	           }
	           while(rückgabebetrag >= 1.00) // 1 EURO-Münzen
	           {
	        	  System.out.println("1 EURO");
		          rückgabebetrag -= 1.00;
		          rückgabebetrag= Math.round(rückgabebetrag *100.0) /100.0 ;
	           }
	           while(rückgabebetrag >= 0.50) // 50 CENT-Münzen
	           {
	        	  System.out.println("50 CENT");
		          rückgabebetrag -= 0.50;
		          rückgabebetrag= Math.round(rückgabebetrag *100.0) /100.0 ;
	           }
	           while(rückgabebetrag >= 0.20) // 20 CENT-Münzen
	           {
	        	  System.out.println("20 CENT");
	 	          rückgabebetrag -= 0.20;
	 	          rückgabebetrag= Math.round(rückgabebetrag *100.0) /100.0 ;
	           }
	           while(rückgabebetrag >= 0.10) // 10 CENT-Münzen
	           {
	        	  System.out.println("10 CENT");
		          rückgabebetrag -= 0.10;
		          rückgabebetrag= Math.round(rückgabebetrag *100.0) /100.0 ;
	           }
	           while(rückgabebetrag >= 0.05)// 5 CENT-Münzen
	           {
	        	  System.out.println("5 CENT");
	 	          rückgabebetrag -= 0.05;
	 	          rückgabebetrag= Math.round(rückgabebetrag *100.00) /100.00 ;
	           }
	       }

	       System.out.println("\nVergessen Sie nicht, den Fahrschein\n"+
	                          "vor Fahrtantritt entwerten zu lassen!\n"+
	                          "Wir wünschen Ihnen eine gute Fahrt.");
		
		return rückgabebetrag;
	}
	
    public static void main(String[] args)
    {
      
        double eingezahlterGesamtbetrag =0;
        double zuZahlenderBetrag = 0; 
       // double eingezahlterGesamtbetrag;
        //double eingeworfeneMünze;
        double rückgabebetrag = 0;
        double einzelpreis = 1.70;
        double tageskarte = 3.00;
        double gruppenkarte = 8.50;
        
    	
    	while(true) {
    		
    		
    		
    		zuZahlenderBetrag = fahrkartenBestellungErfassen(zuZahlenderBetrag);
    		
    		
    		/*
    		System.out.println("0. exit");
    		System.out.println("1. Einzelfahrscheine		(st. 1,70€)");
    		System.out.println("2. Tageskarte		 	(st. 3,10€)");
    		System.out.println("3. Gruppenkarte		 	(st. 8,50€)");
    		*/
    		
    		
    		
  
       
     /*  public static String format(double i) {
       	DecimalFormat f = new DecimalFormat("#0.00");
       	double toFormat = ((double)Math.round(i*100))/100;
       	return f.format(toFormat);
       }
*/
       
       zuZahlenderBetrag = fahrkartenErstellungErfassen(zuZahlenderBetrag);       
       System.out.printf( "%.2f€ ",  zuZahlenderBetrag);
       
       // Geldeinwurf
       // -----------
      
      eingezahlterGesamtbetrag = fahrkartenBezahlen(zuZahlenderBetrag,eingezahlterGesamtbetrag);
      System.out.printf( "%.2f€ ", eingezahlterGesamtbetrag);
       
       // Fahrscheinausgabe
       // -----------------
      fahrkartenAusgeben();
      
      //Warten
      //-------------------
      warten(300);
      
       // Rückgeldberechnung und -Ausgabe
       // -------------------------------
      rückgabebetrag = rueckgeldAusgeben(rückgabebetrag, eingezahlterGesamtbetrag, zuZahlenderBetrag);
      System.out.println("\n");
    	}
    }
}
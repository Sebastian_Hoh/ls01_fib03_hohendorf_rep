
public class Aufgabe3 {

	public static void main(String[] args) {
		// TODO Auto-generated method stub
		
		System.out.printf( "%-12s|", "Fahrenheit" );
		System.out.printf( "%10s", "Celsius" ); 
		
		System.out.print("\n------------------------");
		
		System.out.printf( "\n-%-11s|", "20" );
		System.out.printf( "%10s", "-28.89" );
		
		System.out.printf( "\n-%-11s|", "10" );
		System.out.printf( "%10s", "-23.33" );
		
		System.out.printf( "\n+%-11s|", "0" );
		System.out.printf( "%10s", "-17.78" );
		
		System.out.printf( "\n+%-11s|", "10" );
		System.out.printf( "%10s", "-6.67" );
		
		System.out.printf( "\n+%-11s|", "30" );
		System.out.printf( "%10s", "-1.11" );
	}

}

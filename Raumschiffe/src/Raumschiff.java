import java.util.List;
import java.util.ArrayList;
import java.util.Scanner;

public class Raumschiff {

	
	private String name;
	private double energy;
	private double schutzschild;
	private double lebenserhaltungssystem;
	private int photonentorpedo;
	private int phaserkanone;
	private double h�lle;
	private int androiden;
	private String logBuch;
	private ArrayList<String> broadcastkommunitkator;
	private List<Ladung> ladungsliste = new ArrayList<Ladung>();
	
	
	// Constructor
	
		public Raumschiff(String name, double energy, double schutzschild, double lebenserhaltungssystem,
				int photonentorpedo, int phaserkanone, double h�lle, int androiden, String logBuch,
				ArrayList<String> broadcastkommunitkator,List<Ladung> ladungsliste) {
			super();
			this.name = name;
			this.energy = energy;
			this.schutzschild = schutzschild;
			this.lebenserhaltungssystem = lebenserhaltungssystem;
			this.photonentorpedo = photonentorpedo;
			this.phaserkanone = phaserkanone;
			this.h�lle = h�lle;
			this.androiden = androiden;
			this.logBuch = logBuch;
			this.broadcastkommunitkator = broadcastkommunitkator;
			this.ladungsliste = ladungsliste;
		}
	
		
	// Getter & Setter
	
	public String getName() {
		return name;
	}

	public void setName(String name) {
		this.name = name;
	}

	public double getEnergy() {
		return energy;
	}

	public void setEnergy(double energy) {
		this.energy = energy;
	}

	public double getSchutzschild() {
		return schutzschild;
	}

	public void setSchutzschild(double schutzschild) {
		this.schutzschild = schutzschild;
	}

	public double getLebenserhaltungssystem() {
		return lebenserhaltungssystem;
	}

	public void setLebenserhaltungssystem(double lebenserhaltungssystem) {
		this.lebenserhaltungssystem = lebenserhaltungssystem;
	}

	public int getPhotonentorpedo() {
		return photonentorpedo;
	}

	public void setPhotonentorpedo(int photonentorpedo) {
		this.photonentorpedo = photonentorpedo;
	}

	public int getPhaserkanone() {
		return phaserkanone;
	}

	public void setPhaserkanone(int phaserkanone) {
		this.phaserkanone = phaserkanone;
	}

	public double getH�lle() {
		return h�lle;
	}

	public void setH�lle(double h�lle) {
		this.h�lle = h�lle;
	}

	public int getAndroiden() {
		return androiden;
	}

	public void setAndroiden(int ansroiden) {
		this.androiden = ansroiden;
	}

	public String getLogBuch() {
		return logBuch;
	}

	public void setLogBuch(String logBuch) {
		this.logBuch = logBuch;
	}

	public ArrayList<String> getBroadcastkommunitkator() {
		return broadcastkommunitkator;
	}

	public void setBroadcastkommunitkator(ArrayList<String> broadcastkommunitkator) {
		this.broadcastkommunitkator = broadcastkommunitkator;
	}

	
	// Start der Methoden 
	
	
	Scanner scan = new Scanner(System.in);
	

	// Photonentorpedo abfeuern
	public static void photonentorpedoAbfeuern2(Raumschiff raumschiff, int photonentorpedo) {
		
		if (photonentorpedo == 0 ) {
			System.out.println("-=*Click*=-");
		} else {
			photonentorpedo = photonentorpedo - 1;
			System.out.println("Photonentorpedos abgefeuert!");
			treffernBemerkt(raumschiff);
		}
	}
	
	public static void photonentorpedoAbfeuern(Raumschiff raumschiff1, Raumschiff raumschiff2) {
		
		if (raumschiff1.photonentorpedo == 0 ) {
			System.out.println("-=*Click*=-");
		} else {
			raumschiff1.photonentorpedo = raumschiff1.photonentorpedo - 1;
			System.out.println("Photonentorpedos abgefeuert!");
			treffernBemerkt(raumschiff2);
		}
	}
	
	// PhaserKanone abfeuern 
	public static void phaserKanoneAbfeuern2(Raumschiff raumschiff, double energy) {
		
		if (raumschiff.energy < 0.5) {
			System.out.println("-=*Click*=-");
		} else {
			
			raumschiff.energy = -0.5; 
			System.out.println("Phaserkanone abgeschossen");
			treffernBemerkt(raumschiff);
		}
	}
	
	// PhaserKanone abfeuern
	public static void phaserKanoneAbfeuern(Raumschiff raumschiff1, Raumschiff raumschiff2) {
			
			if (raumschiff1.energy < 0.5) {
				System.out.println("-=*Click*=-");
			} else {
				
				raumschiff1.energy -= 0.5; 
				System.out.println("Phaserkanone abgeschossen");
				treffernBemerkt(raumschiff2);
			}
		}
	
	// Treffer Bemerkt melden
	public static void treffernBemerkt(Raumschiff raumschiff) {
		
		if (raumschiff.h�lle <= 0) {
			raumschiff.lebenserhaltungssystem = 0;
			System.out.println("Lebenserhaltungssysteme sind zerst�rt!");
		} else if (raumschiff.energy <= 0 || raumschiff.schutzschild <= 0) {
			raumschiff.h�lle = raumschiff.h�lle - 0.5;
			raumschiff.energy = raumschiff.energy - 0.5;
		} else {
			raumschiff.schutzschild = raumschiff.schutzschild - 0.5;
		}	
		
	}
	
	
	// Nachricht an alle Schiffe senden
	public static void nachrichtAnAlle(ArrayList<String> broadcastkommunitkator) {
		
		Scanner scan = new Scanner(System.in);
		
		System.out.println("Verfasse deine Nachricht: " );
		broadcastkommunitkator.add(scan.nextLine());
		
	}
	
	
	// alle Nachrichten ausgeben	
	public static void alleNachrichten(ArrayList<String> broadcastkommunitkator) {
		
		for (int i = 0; i < broadcastkommunitkator.size(); i++) {
			System.out.println(broadcastkommunitkator.get(i));
		}
	}
		
	// reperatur des schiffes (Bis zur Unendlichkeit und noch viel Weiter!!)
	public static void reperaturU(Raumschiff raumschiff) {
		
		Scanner scan = new Scanner(System.in);
		 boolean bSchutz = true;
		 boolean bEnergy = true;
		 boolean bH�lle = true;
		 int treue = 3;
		
		if (raumschiff.schutzschild < 1.0) {
			bSchutz = false;
			treue--;
		}
		if (raumschiff.energy < 1.0) {
			bEnergy = false;
			treue--;
		}
		if (raumschiff.h�lle < 1.0) {
			bH�lle = false;
			treue--;
		}
		
		if(raumschiff.androiden >= 0) {
			if(bSchutz == false || bEnergy == false || bH�lle == false) {
				System.out.println("Wieviele Andoroiden willst du einsetzen? ");
		 
				int ad = scan.nextInt();
		 
				if (ad >= raumschiff.getAndroiden()) {
					ad = raumschiff.androiden;
					raumschiff.androiden = - raumschiff.androiden;
				} else {
					raumschiff.androiden = raumschiff.androiden - ad; 
				}
		 	
				double radom = Math.random();
				
				System.out.println("ad: " + ad + "\nRadomezahl: " + radom + "\nTrue gestzte Sektoren: " + treue);
				
				double eh = radom * ad / treue;
				ad = 0;
				System.out.println("Die Besch�digten Bereiche wurden durch Opfer deiner Androiden zu " + eh + " repariert");
				
				if (!bSchutz) {
					raumschiff.schutzschild = + eh;
				}
				if (!bEnergy) {
					raumschiff.energy = + eh;
				}
				if (!bH�lle) {
					raumschiff.h�lle = + eh;
				}
			
				eh = 0;
				
			}
		}
	}

	// reperatur des schiffes
	public static void reperatur(Raumschiff raumschiff) {
		
		Scanner scan = new Scanner(System.in);
		 boolean bSchutz = true;
		 boolean bEnergy = true;
		 boolean bH�lle = true;
		 int treue = 0;
		
		if (raumschiff.schutzschild < 1.0) {
			bSchutz = false;
			treue++;
		}
		if (raumschiff.energy < 1.0) {
			bEnergy = false;
			treue++;
		}
		if (raumschiff.h�lle < 1.0) {
			bH�lle = false;
			treue++;
		}
		
		if(raumschiff.androiden >= 0) {
			if(bSchutz == false || bEnergy == false || bH�lle == false) {
				System.out.println("Wieviele Andoroiden willst du einsetzen? ");
		 
				int ad = scan.nextInt();
		 
				if (ad >= raumschiff.getAndroiden()) {
					ad = raumschiff.androiden;
					raumschiff.androiden -= raumschiff.androiden;
				} else {
					raumschiff.androiden = raumschiff.androiden - ad; 
				}
		 	
				double radom = Math.random();
				
				System.out.println("ad: " + ad + "\nRadomezahl: " + radom + "\nTrue gestzte Sektoren: " + treue);
				
				double eh = radom * ad / treue;
				ad = 0;
				System.out.println("Die Besch�digten Bereiche wurden durch Opfer deiner Androiden zu " + eh + " repariert");
				
				if (!bSchutz) {
					raumschiff.schutzschild = raumschiff.schutzschild + eh;
				}
				if (!bEnergy) {
					raumschiff.energy = raumschiff.energy + eh;
				}
				if (!bH�lle) {
					raumschiff.h�lle  += eh;
				}
			
				eh = 0;
				
			}
		}
	}
	
	// Status des Schiffes ausgeben
	public void Status(Raumschiff raumschiff) {
		
		System.out.println("Name: " + raumschiff.getName() + "\nEnergy: " + raumschiff.getEnergy() + "\nSchutzschild: " + raumschiff.getSchutzschild() 
				+ "\nLebenserhaltungssystem: " + raumschiff.getLebenserhaltungssystem() + "\nPhotonentorpedos: " + raumschiff.getPhotonentorpedo() + 
				"\nH�lle: " + raumschiff.getH�lle() + "\nAndroiden: " + raumschiff.getAndroiden() +  "\n" );
		
		//System.out.println(Raumschiff.class.getConstructors());
	}
	
	// Ladungsverzeichnisse Anzeigen
	public static void ladungsverzeichnis(List<Ladung> ladungsliste) {
		
		for (int i = 0; i < ladungsliste.size(); i++) {
		//System.out.println("Name: " + ladung.getlName() + "\nAnzahl: " + ladung.getAnzahl() + "\nGrund: " + ladung.getGrund() + "\n");
		System.out.println("Name: " + ladungsliste.get(i).getlName() + "\nAnzahl: " + ladungsliste.get(i).getAnzahl() + "\nGrund: " + ladungsliste.get(i).getGrund() + "\n");

		}
	}
	
	/**  Rohr nachladen mit Photonentorpedos */
	public static void rohrNachladen(Raumschiff raumschiff, List<Ladung> ladungsliste) {
		
		Scanner scan = new Scanner(System.in);
		int tLaden = 0;
		
		System.out.println("Wiveiele Torpedos m�chtest du laden? ");
		tLaden = scan.nextInt();
		
		for (int i = 0; i < ladungsliste.size(); i++) {
			//System.out.println(ladungsliste.get(i).getlName());
			if(ladungsliste.get(i).getlName() == "Photonentorpedo") {
				
				if (ladungsliste.get(i).getAnzahl() == 0) {
					
					System.out.println("-=*Click*=-");
					System.out.println("Du hast keine Torpedos mehr! ");
					
				} else if(tLaden > ladungsliste.get(i).getAnzahl()) {
					
					tLaden = ladungsliste.get(i).getAnzahl(); 
					raumschiff.setPhotonentorpedo(raumschiff.getPhotonentorpedo() + tLaden);
					ladungsliste.get(i).setAnzahl(0); 
					System.out.println("Alle Photonentorpedos wurden nachge�llt");
					
				} else if(tLaden <= ladungsliste.get(i).getAnzahl()) {
					
					raumschiff.setPhotonentorpedo(raumschiff.getPhotonentorpedo() + tLaden);
					ladungsliste.get(i).setAnzahl(ladungsliste.get(i).getAnzahl()-tLaden); 
					System.out.println("Photonentorpedos wurden nachge�llt");
					
					}
			} 
			
		}
		
		
	}
	
	// Schiff Ladung beladung
	public static void beladen() {
		
	}
	
	// AddLadung???
	public static void addLadung(List<Ladung> ladungsliste){
		
		Scanner scan = new Scanner(System.in);
		
		Ladung neueLadung  = new Ladung();
		
		System.out.println("Geb einen Namen f�r die Ladung ein: ");
		String lName = scan.nextLine();
		neueLadung.setlName(lName);
		System.out.println("Geb die Anzahl der Ladungen ein: ");
		int anzahl = scan.nextInt();
		neueLadung.setAnzahl(anzahl);
		System.out.println("Geb einen Grund ein, wof�r die ladung gebraucht wird: ");
		String grund = scan.next();
		neueLadung.setGrund(grund);
		
		//String bra = scan.nextLine(); 
		
		ladungsliste.add(neueLadung);
		
	}
	
	// Ladungsverzeichnisse Leeren
	public static void ladungsverzeichnisLeeren(List<Ladung> ladungsliste) {
		for (int i = 0; i < ladungsliste.size(); i++) {
			if(ladungsliste.get(i).getAnzahl() == 0) {
				ladungsliste.remove(i);
				System.out.println("Die Liste der Ladungen wurde geleert mit dem was nicht mehr Vorr�tig ist! ");
			} 
		}
	}
	

	// Main	
	public static void main(String[] args) {
		// TODO Auto-generated method stub
		ArrayList<String> broadcastkommunitkator = new ArrayList<String>();
		List<Ladung> ladungsliste = new ArrayList<Ladung>();
		ladungsliste.add(new Ladung("Photonentorpedo", 3 , "Waffe"));
		ladungsliste.add(new Ladung("Ende", 5 , "warum nicht?"));
		
		Raumschiff enterprise = new Raumschiff("Enterprise ", 1.0, 1.0, 1.0, 5, 3, 1.0, 3, "LogBuch", broadcastkommunitkator,ladungsliste);
		Raumschiff denise = new Raumschiff("Denise Ende ", 0.4, 0.7, 1.0, 5, 3, 0.5, 3, "LogBuch", broadcastkommunitkator,ladungsliste);
		Ladung neueLadung = new Ladung();
		
		Scanner scan = new Scanner(System.in);

		System.out.println("Geb einen Daniel ein; ");	 
		
		while(true) {
			
		
		String daniel = scan.nextLine();
			 
		switch (daniel) {
		
		case "0":
			
			enterprise.Status(enterprise);
			
			break;
		case "1":
			
			ladungsverzeichnis(ladungsliste);
			
			break;
		case "2":
			
			nachrichtAnAlle(broadcastkommunitkator);
			
			
			break;
		case "3":
			
			alleNachrichten(broadcastkommunitkator);
			
			break;
		case "4":
			
			reperatur(enterprise);
			
			break;
		case "5":
			
			reperatur(denise);
			
			break;
		case "6":
			
			photonentorpedoAbfeuern(denise, enterprise);
			
			break;
		case "7":
			
			denise.Status(denise);
			
			break;
		case "8":
			
			addLadung(ladungsliste);
			
			break;
		case "9":
			
			ladungsverzeichnisLeeren(ladungsliste);
			
			break;
		case "10":
			
			rohrNachladen(enterprise, ladungsliste);
	
			break;
		case "11":
			
			reperaturU(denise);
			
			break;

		default:
			
			System.out.println("Geb einen neuen Daniel ein; ");
			
			
			break;
		}
	 }	
		
		
		
		
		
		
		
		
	}
	
}

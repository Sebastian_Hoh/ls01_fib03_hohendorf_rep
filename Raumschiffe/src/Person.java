
public class Person {

	private String pName;
	private int kapitšnSeit;
	private String Geschlecht;
	private boolean mehrereSchiffe;
	
	
	// Constructor

	public Person(String pName, int kapitšnSeit, String geschlecht, boolean mehrereSchiffe) {
		super();
		this.pName = pName;
		this.kapitšnSeit = kapitšnSeit;
		this.Geschlecht = geschlecht;
		this.mehrereSchiffe = mehrereSchiffe;
	}

	// Getter & Setter
	
	public String getpName() {
		return pName;
	}

	public void setpName(String pName) {
		this.pName = pName;
	}

	public int getKapitšnSeit() {
		return kapitšnSeit;
	}

	public void setKapitšnSeit(int kapitšnSeit) {
		this.kapitšnSeit = kapitšnSeit;
	}

	public String getGeschlecht() {
		return Geschlecht;
	}

	public void setGeschlecht(String geschlecht) {
		this.Geschlecht = geschlecht;
	}

	public boolean isMehrereSchiffe() {
		return mehrereSchiffe;
	}

	public void setMehrereSchiffe(boolean mehrereSchiffe) {
		this.mehrereSchiffe = mehrereSchiffe;
	}
	
	// Methoden
	
	
	// Main
	
	public static void main(String[] args) {
		// TODO Auto-generated method stub

	}
}


